import axios from 'axios';
import Config from './Config';

let baseUrlConfig = Config.backendUrl;
export const baseUrl = baseUrlConfig;
export default {
  fetchRecords() {
    let url = baseUrl+`/fetch-record`;
    return axios.get(url);
  },
  storeRecord(formData) {
    let url = baseUrl+`/store-record`;
    return axios.post(url , formData)
  },
};
