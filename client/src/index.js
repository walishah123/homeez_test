import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/layouts/default';
ReactDOM.render(
    <App />,
    document.getElementById('root')
);
