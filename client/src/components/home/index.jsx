import React, {useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import Request from "../../global/Requests";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import {Link} from "react-router-dom";
import Box from '@material-ui/core/Box';


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));
export default function Form() {
    const classes = useStyles();
    const [text , setTextData] = useState('');
    const [open, setOpen] = React.useState(false);
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };
    useEffect(() => {
        console.log('index page init .. !');
    }, []);
    const handleTextInput = (e) => {
        setTextData(e.target.value);
    }
    const submitForm = () => {
        let formData = {
            info:text,
        };
        Request.storeRecord(formData).then((response) => {
            setTextData('');
            setOpen(true);
        }).catch((error) => {
           console.log('error');
           console.log(error);
        });
    }
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.root}>
                <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success">
                        Data Saved Successfully ..!
                    </Alert>
                </Snackbar>
                <form noValidate>
                    <TextField
                        required
                        fullWidth
                        id="email"
                        label="Enter Info"
                        name="email"
                        autoFocus
                        value={text}
                        onChange={handleTextInput}
                    />
                    <Box
                        display="flex"
                        flexWrap="warp"
                        justifyContent="space-between"
                        p={1}
                        bgcolor="background.paper"
                        sx={{ maxWidth: '100%' }}
                    >
                        <Box p={1}>
                            <Button
                                type="button"
                                variant="outlined"
                                color="primary"
                                href="#outlined-buttons"
                                onClick={submitForm}
                            >
                                Submit
                            </Button>
                        </Box>
                        <Box p={1}>
                            <Button
                                type="button"
                                fullWidth
                                variant="contained" color="secondary"
                            >
                                <Link to="/show" style={{
                                    textDecoration:'none'
                                }}><b style={{
                                    color:'#fff'
                                }}>Show Table</b></Link>
                            </Button>
                        </Box>
                    </Box>
                </form>
            </div>
        </Container>
    );
}
