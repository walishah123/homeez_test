import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Show from '../list/listing';
import Index from '../home/index';

export default function MainLayout() {
    return (
        <Router>
            <div>
                <ul className="headerSection">
                    <li>
                        <h1>Homeez Test</h1>
                    </li>
                </ul>
                <Switch>
                    <Route exact path="/show">
                        <Show />
                    </Route>
                    <Route path="/">
                        <Index />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}
