import React , {useState , useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Request from "../../global/Requests";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import Box from '@material-ui/core/Box';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function createData(XID, X_TableInfo, Xtable_Valid , created_at) {
    return {XID, X_TableInfo, Xtable_Valid , created_at};
}

function ShowTable({props}) {
    const classes = useStyles();
    const [rows , setRows] = useState([]);
    useEffect(() => {
        Request.fetchRecords().then((response) => {
            const arr = [];
            response.data.data.map((dt) => {
                let Xtable_Valid = 'false';
                if(dt.Xtable_Valid == true) {
                    Xtable_Valid = 'true';
                }
                arr.push(createData(dt.XID,dt.X_TableInfo,Xtable_Valid,dt.createdAt));
            });
            setRows(arr);
        }).catch((error) => {
           console.log('error');
           console.log(error);
        });
    }, []);
    return (
        <div>
            <div>
                <Box
                    display="flex"
                    flexWrap="warp"
                    justifyContent="flex-end"
                    p={1}
                    bgcolor="background.paper"
                    sx={{ maxWidth: '100%' }}
                >
                    <Button
                        type="button"
                        variant="contained" color="secondary"
                    >
                        <Link to="/" style={{
                            textDecoration:'none'
                        }}><b style={{
                            color:'#fff',textDecoration:'none'
                        }}>Home</b></Link>
                    </Button>
                </Box>
            </div>
            <hr />
            <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell align="center" style={{fontWeight:'bold'}}>Info</TableCell>
                        <TableCell align="center" style={{fontWeight:'bold'}}>Valid</TableCell>
                        <TableCell align="center" style={{fontWeight:'bold'}}>Date</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.XID}>
                            <TableCell component="th" scope="row">
                                {row.XID}
                            </TableCell>
                            <TableCell align="center">{row.X_TableInfo}</TableCell>
                            <TableCell align="center">{row.Xtable_Valid}</TableCell>
                            <TableCell align="center">{row.created_at}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
        </div>
    );
}

export default ShowTable;
