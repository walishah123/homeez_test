**Project Guide**

**Backend Guide**

**1-** cd server

**2-** npm install	

**3-** nodemon server.js

**4-** cd server/database/db 

database connection using mongoose

-------------------------------------------

**Frontend Guide** 

**1-** cd frontend 

**2-** npm install

**3-** npm start

**4-** able to change port using package.json file **script inside start = PORT=**

**5-** global folder inside file **Config.js where update config** 

**6-** global folder inside **Requests.js where you handle project requests**


