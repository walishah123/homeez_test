/* require database */ require('./database/db');
const express = require('express');
const Router  = require('./routes/route');
var app = express();
const bodyParser = require('body-parser')
var cors = require('cors');
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())
const PORT = 8080;
app.listen(PORT);
console.log('node server = http:://localhost:' + PORT + '/api');
app.get('/', (request, response) => {
    response.send('Hello Node I am Here To Run You .. !');
});
app.use('/api', Router);
