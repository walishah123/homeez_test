const express = require('express')
const Data  = require('../controllers/info.controller');
const route = express.Router()
route.post('/store-record', Data.storeRecord);
route.get('/fetch-record', Data.fetchRecord);
module.exports = route
