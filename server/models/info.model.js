const mongoose = require('mongoose');
const AutoIncrement = require('mongoose-sequence')(mongoose);
const InformationSchema = mongoose.Schema({
        XID: { type: Number, default: 0 },
        Xtable_Valid: { type: Boolean, default: true },
        X_TableInfo: { type: String }
    }, { timestamps: true },
)
InformationSchema.plugin(AutoIncrement, {inc_field: 'XID'});
module.exports = mongoose.model('information', InformationSchema)
