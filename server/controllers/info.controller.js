const mongoose = require('mongoose');
const Information = mongoose.model('information');
const fetchData = require('../models/info.model')

storeRecord = async  (request, response) => {
   let information = new Information();
   information.X_TableInfo = request.body.info;
   information.save((error, doc) => {
      if (!error) {
         return response.status(200).json({ success: true, data: 'data saves success' })
      } else {
         return response.status(400).json({ success: false, error: error })
      }
   });
}

fetchRecord = async  (request, response) => {
   await fetchData.find({}, (error, infoResults) => {
      if (error) {
         return response.status(400).json({ success: false, error: error })
      }
      if (!infoResults.length) {
         return response
             .status(404)
             .json({ success: false, error: `no results found` })
      }
      return response.status(200).json({ success: true, data: infoResults })
   }).catch(error => console.log(error))
}

module.exports = {
   storeRecord,
   fetchRecord
};
